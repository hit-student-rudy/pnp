#!/usr/bin/env python

#import all of the needed libraries
from __future__ import print_function
import roslib
roslib.load_manifest('opencv')
import sys
import rospy
import cv2
import numpy as np
from geometry_msgs.msg import Point, Quaternion, Pose
from sensor_msgs.msg import Image
from std_msgs.msg import String, Float64 
from cv_bridge import CvBridge

class get_coordinates:

  #initiating the subscriptions and publishers
  def __init__(self):
    self.bridge = CvBridge()
    self.image_rgb = rospy.Subscriber("xtion/rgb/image_raw",Image,self.callback_rgb) 
    self.image_depth = rospy.Subscriber("xtion/depth_registered/image_raw",Image,self.callback_depth)
    self.send_pose = rospy.Publisher('object_pose_robot', Pose, queue_size=10 )
    rospy.sleep(2)
  
  #Depth needs it's own callback to process the image for OpenCV, use 'self.' to use it in other def's
  def callback_depth(self,data):
    self.cv_depth= self.bridge.imgmsg_to_cv2(data,  desired_encoding="passthrough")
    

  #Callback for rgb and body of the code
  def callback_rgb(self,data):
    cv_rgb = self.bridge.imgmsg_to_cv2(data, 'bgr8')

    #Transformation rgb to hsv, other form of color geometry 
    hsv = cv2.cvtColor(cv_rgb, cv2.COLOR_BGR2HSV)
    
    #Decide wich colour will be detected, here green.
    lower_hsv = np.array([10, 130, 80])
    higher_hsv = np.array([30, 255, 255])

    #Making a mask and filtering for the colour, you can see if the colour is detected
    mask = cv2.inRange(hsv, lower_hsv, higher_hsv)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT,(1,1)), iterations=10)
    contour = cv2.findContours(mask.copy(), 
                                cv2.RETR_EXTERNAL, 
                                cv2.CHAIN_APPROX_SIMPLE) [-2]

    for cnt in contour:
      #If the contour is larger than 1000 pixels, and object is identified as an object
      if  cv2.contourArea(cnt) > 10 : #1000

        #Determine outer borders of object and drawing rectangle 
        area = max(contour, key=cv2.contourArea)
        xg,yg,wg,hg = cv2.boundingRect(area)
        cv2.rectangle(cv_rgb,(xg,yg),(xg+wg, yg+hg),(0,255,0),2)

        #determining the middle of the rectangle 
        self.x_px = xg+(wg/2)
        self.y_px = yg+(hg/2)
        self.z = self.cv_depth[self.y_px,self.x_px]
        cv2.circle(cv_rgb, (self.x_px,self.y_px), 1,(255,50,0),2)

        #calculating the x and y distance in metres from the RGB camera
        x = (self.x_px - 320.5)/522.1910329546544*self.z
        y = (self.y_px - 240.5)/522.1910329546544*self.z

        #making a point, then a pose, publish the pose
        quat_tf = [0.0, 0.0, 0.0, 1.0]
        self.point = Point(x=x,y=y,z=self.z )
        self.pose = Pose(position = self.point, orientation = Quaternion(quat_tf[0], quat_tf[1], quat_tf[2], quat_tf[3])) 
        self.send_pose.publish(self.pose) 

    #showing the video output
    cv2.imshow("RGB", cv_rgb)
    #cv2.imshow("mask", mask)
    #cv2.imshow("Depth", self.cv_depth)
    cv2.waitKey(3)

  
def main(args):
  rospy.init_node('M1_object_detection', anonymous=True)
  get_coordinates()
  rospy.spin()  
  cv2.destroyAllWindows()

#call main
if __name__ == '__main__':
  print("M1_object_detection.py started")
  main(sys.argv)
  print("M1_object_detection.py terminated")
    
    

