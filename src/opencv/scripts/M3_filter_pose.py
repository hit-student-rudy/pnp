#!/usr/bin/env python
#This code filters the output and makes a tf marker for this pose, it publishes the pose the 'opt_filtered' topic
from __future__ import print_function
import roslib
import sys
import rospy
import tf
import numpy as np
from math import sqrt
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionGoal
from geometry_msgs.msg import PoseStamped, TransformStamped, Pose, Point, Quaternion

class filter_pose:
    

    #initiating the subscriptions and publishers
    def __init__(self):
        self.iter_x, self.iter_y, self.iter_z = 0,0,0
        self.filtered_x, self.filtered_y, self.filtered_z = 0,0,0
        self.tb = tf.TransformBroadcaster()
        self.send_pose = rospy.Publisher('object_pose_filtered', Pose, queue_size=10)
        rospy.Subscriber('object_pose_map', PoseStamped, self.callback)
       
    def callback(self, data):
        #Smooth the point
        self.filtered_x, self.iter_x = self.filter(data.pose.position.x, self.iter_x, self.filtered_x)
        self.filtered_y, self.iter_y = self.filter(data.pose.position.y, self.iter_y, self.filtered_y)
        self.filtered_z, self.iter_z = self.filter(data.pose.position.z, self.iter_z, self.filtered_z)
        quat = data.pose.orientation
        
        #Send the transformation to tf. Needed for readout in RVIZ
        filtered_point = Point(x = self.filtered_x, y = self.filtered_y, z = self.filtered_z)
        pose_filtered = Pose(position = filtered_point, orientation = quat)
        self.send_pose.publish(pose_filtered)
        self.make_frame(filtered_point, quat)

    def make_frame(self, point, quat):
        filtered_marker = TransformStamped()
        filtered_marker.header.frame_id = 'map'
        filtered_marker.child_frame_id = 'object_pose_filtered'
        filtered_marker.header.stamp = rospy.Time.now()
        filtered_marker.transform.translation = point
        filtered_marker.transform.rotation = quat
        self.tb.sendTransformMessage(filtered_marker)
        return()

    def filter(self,X,i,old):
        if i == 0:
            old = X
        else: 
            old = old
        
        smoothed = old + 0.3*(X - old)
        i += 1
        return smoothed, i

    def normalizeQuat(self,quat):
        # normalize
        normalizedQuat = []
        for q in quat: 
            norm = sqrt(q**2)
            normalizedQuat.append(norm)
    
        return normalizedQuat

def main(args):
  rospy.init_node('M3_filter_pose', anonymous=True)
  filter_pose()
  rospy.spin()  

#call main
if __name__ == '__main__':
    print('M3_filter_pose.py started')
    main(sys.argv)
    print('M3_filter_pose.py terminated')