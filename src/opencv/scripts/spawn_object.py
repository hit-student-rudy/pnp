#!/usr/bin/env python
from __future__ import print_function
import roslib
import rospy
import sys
from moveit_msgs.msg import  CollisionObject
from moveit_commander import PlanningSceneInterface
from geometry_msgs.msg import PoseStamped


class place_object:

    #initiating the subscriptions and publishers
    def __init__(self):
        scene = PlanningSceneInterface()
        self.place_it(scene)

    def place_it(self, scene):
        box = PoseStamped()
        box.header.frame_id = "map"

        rospy.sleep(2)
        box.pose.position.x = 3.0
        box.pose.orientation.w = 1.0
        scene.add_box("object", box, size=(0.1,0.1,0.1))

def main(args):
    rospy.init_node('spawn_object', anonymous=True)
    place_object()
    rospy.spin()  

#call main
if __name__ == '__main__':
    print("spawn_object.py started")
    main(sys.argv)
    print("spawn_object.py terminated")
    