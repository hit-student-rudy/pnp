#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf
import actionlib
import numpy as np
from actionlib_msgs.msg import GoalStatusArray, GoalStatus
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from std_msgs.msg import Bool, Int16
tries = []

class set_nav_goal:

    #initiating the subscriptions and publishers
    def __init__(self):
        
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        self.client.wait_for_server()
        rospy.Subscriber('object_pose_filtered', Pose, self.callback_pick) 
        rospy.Subscriber('nav_place', Bool, self.callback_drop)
        self.nav_result = rospy.Publisher('nav_result', Bool, queue_size=2)
        self.drop_it = rospy.Publisher('drop_it', Bool,queue_size=1)
        self.picked = False

    #Boolian waiting for object to be picked up by robot
    def callback_drop(self, data):
        self.picked = data

    #Create a goal for the base to move to, with 0.45 meters distance from the middle of the object
    def callback_pick(self, data):
        pick_goal = MoveBaseGoal()
        pick_goal.target_pose.header.frame_id = 'map'
        pick_goal.target_pose.header.stamp = rospy.Time.now()
        pick_goal.target_pose.pose.position.x = data.position.x + 0.48*np.cos(np.deg2rad(0))
        pick_goal.target_pose.pose.position.y = data.position.y + 0.48*np.sin(np.deg2rad(0))
        pick_goal.target_pose.pose.orientation = Quaternion(0, 0, 1, 0)

        if self.picked == False: self.get_state(pick_goal, data)
        sys.exit(0) #Exit if navigation is done
        

    #Get state of the send goal, act on result
    def get_state(self, goal, data):
        #Send goal
        self.client.send_goal(goal)
        rospy.loginfo("Sending goal...")
        
        if self.client.get_state() == 1:
            rospy.loginfo("Planning...")

        self.client.wait_for_result()
        self.client.cancel_all_goals()
        rospy.sleep(1.0)

        #Goal reached, send go signal to move_arm.py
        if self.client.get_state() == 3:
            self.client.cancel_all_goals()
            self.nav_result.publish(True)
            rospy.loginfo("Reached: pick location")
            self.place_object()

        #Goal cancelled, exit.
        elif self.client.get_state() == 2: 
            self.client.cancel_all_goals()
            rospy.loginfo("Navigation has been cancelled.")
            sys.exit(0)

        #Goal cannot be reached, try other orientations and positions in change_goal
        elif self.client.get_state() == 4:
            self.client.cancel_all_goals()
            rospy.loginfo("Object not reachable. Trying new orientation...")
            self.change_goal(goal,data)


    #Change goal, get state, act on new result
    def change_goal(self,pick_goal,data):
        self.client.cancel_all_goals()
        # Different orientations that can be tried in sets of 45 degrees
        #q0 = Quaternion(0, 0, 1, 0)
        q45 = Quaternion(0, 0, -0.9238795, 0.3826834) 
        q90 = Quaternion(0, 0, -0.7071068, 0.7071068)
        q135 = Quaternion(0, 0, -0.3826834, 0.9238795)  
        q180 = Quaternion(0,0,0,1)  
        q225 = Quaternion(0, 0, 0.3826834, 0.9238795)
        q270 = Quaternion(0, 0, 0.7071068, 0.7071068)
        q315 = Quaternion(0, 0, 0.9238795, 0.3826834)
        circle_orientation = [q45,q90,q135,q180,q225,q270,q315]
        circle_distance = 0 #was 0

        #Try the different orientation and position in a circle around the object. 
        for q in circle_orientation:

            if self.client.get_state() == 4 and len(tries) <= 8:
                #First cancell the old goal
                self.client.cancel_all_goals()

                #Keep track how many options are tried
                circle_distance += 45
                tries.append(circle_distance)
                print("Tried: ", tries) 

                #Cancel old goal, change goal
                pick_goal.target_pose.pose.orientation = q
                pick_goal.target_pose.pose.position.x = data.position.x + 0.47*np.cos(np.deg2rad(circle_distance))
                pick_goal.target_pose.pose.position.y = data.position.y + 0.47*np.sin(np.deg2rad(circle_distance))
                self.client.send_goal(pick_goal)
                self.client.wait_for_result()
                rospy.sleep(1.0)                

            else:
                #Delete the content of tries, break from the loop
                self.client.cancel_all_goals()
                del tries[:]
                break        
       
        #Goal reached, exit
        if self.client.get_state() == 3:
            self.client.cancel_all_goals()
            rospy.loginfo("Goal is reached after changing orientation")

            #Object is reached, try pick up object
            self.nav_result.publish(True) 

            #Wait at place_object off for object to be picked up
            self.place_object()
            sys.exit(0)

        #Goal cancelled, exit
        elif self.client.get_state() == 2:
            self.client.cancel_all_goals()
            rospy.loginfo("Navigation has been cancelled by operator.")
            sys.exit(0)
        
        #Object not reachable, exit
        else:  
            self.client.cancel_all_goals()
            rospy.loginfo("Object still not reachable after trying other orientations. Goal cancelled.")   
            sys.exit(0) 

    def place_object(self):
        while self.picked == False:
            wait = 1
        
        rospy.loginfo('Sending place goal')
        place_goal = MoveBaseGoal()
        place_goal.target_pose.header.frame_id = 'map'
        place_goal.target_pose.header.stamp = rospy.Time.now()
        place_goal.target_pose.pose.position.x = 1 + 0.48*np.cos(np.deg2rad(0))
        place_goal.target_pose.pose.position.y = 0 + 0.48*np.sin(np.deg2rad(0))
        place_goal.target_pose.pose.orientation = Quaternion(0, 0, 1, 0)
        self.client.send_goal(place_goal)
        self.client.wait_for_result()
        rospy.sleep(1.0)

        if self.client.get_state() == 3:
            self.client.cancel_all_goals()
            self.drop_it.publish(True)
            rospy.loginfo("Reached: place location")

        else:
            self.client.cancel_all_goals()
            rospy.loginfo("Drop off location cannot be reached.")
            sys.exit(0)

        self.client.cancel_all_goals()
        sys.exit(0)
    
def main(args):
    rospy.init_node('M4_set_nav_goal', anonymous=True)
    set_nav_goal()
    rospy.spin()  
    sys.exit(0)

#Call main
if __name__ == '__main__':
    print('M4_set_nav_goal.py started')
    main(sys.argv)
    sys.exit(0)  
    print('M4_set_nav_goal.py terminated')