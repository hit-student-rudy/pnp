#!/usr/bin/env python
from __future__ import print_function
import roslib
import rospy
import sys
import copy
import actionlib
from moveit_msgs.msg import DisplayTrajectory
from moveit_commander import RobotCommander, PlanningSceneInterface, MoveGroupCommander, roscpp_initialize
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from std_msgs.msg import Bool
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState

class reach_object:

    #initiating and check
    def __init__(self):


        #Set up sub and pubs
        rospy.loginfo("Starting setup...")
        roscpp_initialize(sys.argv)
        self.nav_place = rospy.Publisher('nav_place', Bool, queue_size=2)
        rospy.Subscriber('object_pose_base_link', PoseStamped, self.callback_gripper_xy_goal)
        rospy.Subscriber('object_pose_filtered', Pose, self.callback_gripper_z_goal)

        #Connecting head and torso
        self.robot = RobotCommander()
        self.scene = PlanningSceneInterface()
        self.move_arm = MoveGroupCommander('arm_torso')
        self.move_head= MoveGroupCommander('head')
        
        #Connecting gripper
        self.joints_gripper = ["gripper_left_finger_joint", "gripper_right_finger_joint"]
        self.gripper_client = actionlib.SimpleActionClient("gripper_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
        self.gripper_client.wait_for_server()

        #Connecting head
        self.joints_head = ["head_1_joint","head_2_joint"]
        self.head_client = actionlib.SimpleActionClient("head_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
        self.head_client.wait_for_server()
        
        rospy.Subscriber('object_pose_base_link', PoseStamped, self.callback_gripper_xy_goal)
        rospy.Subscriber('object_pose_filtered', Pose, self.callback_gripper_z_goal)
        rospy.Subscriber('nav_result', Bool, self.callback_pick_object)
        rospy.Subscriber('drop_it', Bool,self.drop_it)
        self.drop_object_trigger = False
        rospy.loginfo("Setup completed.")

        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        self.client.wait_for_server()

    def callback_gripper_xy_goal(self, xy_goal):
        self.gripper_x = xy_goal.pose.position.x
        self.gripper_y = xy_goal.pose.position.y
    
    def callback_gripper_z_goal(self, z_goal):
        self.gripper_z = z_goal.position.z

    def callback_pick_object(self, go_signal):
        while go_signal == False:
            wait = 1

        self.client.cancel_all_goals()
        object_grasped = False
        tries = 0
        x = 0.0

        #Set positions
        look_up = [0.0,0.0]
        openGripper = [0.045, 0.045]
        closeGripper = [0.001, 0.001]
        gripper_above_object = 0.37
        rospy.sleep(2)
        
        if self.gripper_z < 0.27:
            gripper_at_object = 0.27
        else:
            gripper_at_object = self.gripper_z

        #Arm positions and orientations
        pick_orientation = Quaternion(x=0,y=0.7071068,z=0,w=0.7071068)
        fold = [0.25,-.2,0.61]
        check_orientation = Quaternion(x=0,y=0.7071068,z=0,w=-0.7071068)


        #Plan motion
        while object_grasped == False and tries < 3:
            self.adjust_head([x, -.9])
            
            #Place the arm above the object with opened gripper
            above_object = [self.gripper_x, self.gripper_y, gripper_above_object]
            self.adjust_arm(above_object, pick_orientation)
            self.adjust_gripper(openGripper)
            rospy.sleep(2)

            #Move gripper around the object and close the gripper
            at_object = [self.gripper_x,self.gripper_y, gripper_at_object]
            self.adjust_arm(at_object, pick_orientation)
            rospy.sleep(2)
            self.adjust_gripper(closeGripper)
            rospy.sleep(2)

            #Check if the object is grasped, try again or quit.
            check_position = [self.gripper_x,self.gripper_y,0.5]
            self.adjust_arm(check_position, pick_orientation)
            rospy.sleep(2)
            object_grasped = self.check_object(self.gripper_z)
            self.adjust_arm(fold, check_orientation)
            
            tries += 1
            if tries == 1: x = .5
            if tries == 2: x = -.5

        self.adjust_head(look_up)    
        rospy.loginfo("M5_reach.py finished")
        sys.exit(0)

    def adjust_head(self, position):
        head_trajectory = JointTrajectory()
        head_trajectory.joint_names = self.joints_head
        head_trajectory.points.append(JointTrajectoryPoint())
        head_trajectory.points[0].positions = position
        head_trajectory.points[0].velocities = [0.1 for i in self.joints_head]
        head_trajectory.points[0].accelerations = [0.1 for i in self.joints_head]
        head_trajectory.points[0].time_from_start = rospy.Duration(2.0)

        head_goal = FollowJointTrajectoryGoal()
        head_goal.trajectory = head_trajectory
        head_goal.goal_time_tolerance = rospy.Duration(0.0)

        self.head_client.send_goal(head_goal)
        self.head_client.wait_for_result(rospy.Duration(3.0))

    def adjust_gripper(self, move):
        gripper_trajectory = JointTrajectory()
        gripper_trajectory.joint_names = self.joints_gripper
        gripper_trajectory.points.append(JointTrajectoryPoint())
        gripper_trajectory.points[0].positions = move
        gripper_trajectory.points[0].velocities = [0.1 for i in self.joints_gripper]
        gripper_trajectory.points[0].accelerations = [0.1 for i in self.joints_gripper]
        gripper_trajectory.points[0].time_from_start = rospy.Duration(2.0)

        gripper_goal = FollowJointTrajectoryGoal()
        gripper_goal.trajectory = gripper_trajectory
        gripper_goal.goal_time_tolerance = rospy.Duration(0.0)

        self.gripper_client.send_goal(gripper_goal)
        self.gripper_client.wait_for_result(rospy.Duration(3.0))

    def adjust_arm(self, position, orientation):
        #Plan motion
        pose_target = Pose()
        pose_target.orientation = orientation
        pose_target.position.x = position[0]
        pose_target.position.y = position[1]
        pose_target.position.z = position[2]
        self.move_arm.set_pose_target(pose_target)

        #Compute and execute
        self.move_arm.go(wait=True)
        self.move_arm.stop() #Call stop to make no movement
        self.move_arm.clear_pose_targets()

    def check_object(self, depth):
        orientation = Quaternion(x=0,y=0.7071068,z=0,w=-0.7071068)
        fold = [0.25,-.2,0.61]

        if(depth > 0.27):
            rospy.loginfo("Object successfully grasped")
            self.adjust_head([0.0,-.2])
            self.adjust_arm(fold, orientation)
            self.nav_place.publish(True)
            self.drop_object()
            return(True)
        else:
            rospy.loginfo("Object is not grasped")
            return(False) 
    
    def drop_it(self, data):
        self.drop_object_trigger = data

    def drop_object(self):
        self.client.cancel_all_goals()
        drop_position = [0.55,0.0,1.0]
        drop_orientation = Quaternion(x=0.0,y=0.7071068,z=0,w=0.7071068)

        while self.drop_object_trigger == False:
            wait = 1

        self.nav_place.publish(False)
        rospy.loginfo("Dropping the object")
        self.adjust_head([0.0,-.8])
        self.adjust_arm(drop_position, drop_orientation)
        rospy.sleep(2)
        self.adjust_gripper([0.045, 0.045])
        rospy.sleep(2)
        self.adjust_arm([0.25,-.2,0.61],Quaternion(x=0,y=0.7071068,z=0,w=-0.7071068))
        sys.exit(0)

def main(args):
  rospy.init_node('M5_reach', anonymous=True)
  reach_object()
  rospy.spin()
  sys.exit(0)  

#call main
if __name__ == '__main__':
    print("M5_reach.py started")
    main(sys.argv)
    sys.exit(0)
    print("M5_reach.py terminated")