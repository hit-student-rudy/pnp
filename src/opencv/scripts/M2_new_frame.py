#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf
from numpy import array, sqrt
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, Quaternion

class create_map_frame:

    #initiating the subscriptions and publishers, tl and 
    def __init__(self):
        self.tl = tf.TransformListener()
        self.tb= tf.TransformBroadcaster()
        rospy.Subscriber('object_pose_robot', Pose, self.callback_map)
        rospy.Subscriber('object_pose_robot', Pose, self.callback_link)
        self.send_map = rospy.Publisher('object_pose_map', PoseStamped, queue_size=10)
        self.send_link = rospy.Publisher('object_pose_base_link', PoseStamped, queue_size=10)
        rospy.sleep(2)

    def callback_map(self, data):
        #Copy the pose, make it PoseStamped to transform it from camera frame to map
        map_pose = PoseStamped()
        map_pose.header.frame_id = "xtion_optical_frame"

        #Assign the positions and orientation of the object
        map_pose.pose.position = data.position
        map_pose.pose.orientation = data.orientation
        map_pose.header.stamp = rospy.Time(0)
        map_tf = self.tl.transformPose('map',map_pose)
        _, rot_map = self.tl.lookupTransform('map','base_footprint', rospy.Time(0))
        rot_map = self.normalizeQuat(rot_map)
        map_tf.pose.orientation = Quaternion(x=rot_map[0],y=rot_map[1],z=rot_map[2],w=rot_map[3])
       
        #publish map coordinates on topic for future use
        self.send_map.publish(map_tf) 

        #Create a markerframe of the transformed object to show in rviz
        map_marker = TransformStamped()
        map_marker.header.frame_id = 'map'
        map_marker.child_frame_id = 'object_pose_map'
        map_marker.header.stamp = rospy.Time.now()
        map_marker.transform.translation = map_tf.pose.position
        map_marker.transform.rotation = map_tf.pose.orientation
        self.tb.sendTransformMessage(map_marker)

    #The same code as for the map frame, now for the base_link
    def callback_link(self, data):
        link_pose = PoseStamped()
        link_pose.header.frame_id = "xtion_optical_frame"
        link_pose.pose.position = data.position
        link_pose.pose.orientation = data.orientation
        link_pose.header.stamp = rospy.Time(0)
        link_tf = self.tl.transformPose('base_link',link_pose)
        _, rot_link = self.tl.lookupTransform('map','base_footprint', rospy.Time(0))
        rot_link = self.normalizeQuat(rot_link)
        link_tf.pose.orientation = Quaternion(x=rot_link[0],y=rot_link[1],z=rot_link[2],w=rot_link[3])
        self.send_link.publish(link_tf) 

        link_marker = TransformStamped()
        link_marker.header.frame_id = 'base_link'
        link_marker.child_frame_id = 'object_pose_base_footprint'
        link_marker.header.stamp = rospy.Time.now()
        link_marker.transform.translation = link_tf.pose.position
        link_marker.transform.rotation = link_tf.pose.orientation
        self.tb.sendTransformMessage(link_marker )

    #Normalize, otherwise invalide quaternion
    def normalizeQuat(self,quat):
        # normalize
        normalizedQuat = []
        for q in quat: 
            norm = sqrt(q**2)
            normalizedQuat.append(norm)
            
        return normalizedQuat

def main(args):
  rospy.init_node('M2_new_frame', anonymous=True)
  create_map_frame()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("M2_new_frame.py started")
    main(sys.argv)
    print("M2_new_frame.py terminated")
    