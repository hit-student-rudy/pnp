#!/usr/bin/env python
import rospy
import sys
import math
import numpy as np
import sys
import copy
import moveit_commander
from moveit_msgs.msg import Grasp, DisplayTrajectory
from moveit_commander.conversions import pose_to_list


if __name__ == "__main__":
	rospy.init_node("moveit_trial1")
	robot = moveit_commander.RobotCommander()
	scene = moveit_commander.PlanningSceneInterface()
	group_name = "arm"
	move_group = moveit_commander.MoveGroupCommander(group_name)
	
	display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', DisplayTrajectory,queue_size=20)
    
	#Printing details of robot configuration and group names
	group_names = robot.get_group_names()
	print("Available Planning Groups:", robot.get_group_names())
	planning_frame = move_group.get_planning_frame()
	print("Planning frame: ",planning_frame)
	print("Current states: ",robot.get_current_state() )


	joint_goal = move_group.get_current_joint_values()
	go_to_base_pos = 1
	if go_to_base_pos == 1:
		joint_goal[0] = 0.2
		joint_goal[1] = -1.34
		joint_goal[2] = -0.2
		joint_goal[3] = 1.94
		joint_goal[4] = -1.57
		joint_goal[5] = 1.37
		joint_goal[6] = 0
		move_group.go(joint_goal,wait = True)
		move_group.stop()

	scale = 1
	for i in range(5):
		print(i+1)
		waypoints = []
		wpose = move_group.get_current_pose().pose
		print(wpose.orientation)
		wpose.orientation.z += scale * 0.1
		wpose.orientation.x -= scale * 0.1
		waypoints.append(copy.deepcopy(wpose))
		(plan, fraction) = move_group.compute_cartesian_path(waypoints,0.005,0.0)# jump_threshold
		print(wpose.orientation)

		#execution
		move_group.execute(plan, wait=True)
		print("Done")
	rospy.spin()
