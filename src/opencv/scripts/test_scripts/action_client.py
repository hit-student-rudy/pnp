#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf
import actionlib
import numpy as np
from actionlib_msgs.msg import GoalStatusArray, GoalStatus
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseStamped, Pose, Quaternion
from std_msgs.msg import Int64

tries = []


class set_nav_goal:

    # initiating the subscriptions and publishers
    def __init__(self):
        self.nav_result = rospy.Publisher("nav_result", Int64, queue_size=2)
        self.client = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        self.client.wait_for_server()
        rospy.loginfo("Connected to move_base.")
        # rospy.Subscriber('opt_filtered', Pose, self.callback_pose)
        data = [2,0]
        self.callback_pose(data)

    def callback_pose(self, data):

        # Create poseStamped, needed for nav goal
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = "map"
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = data[0] - 0.3
        goal.target_pose.pose.position.y = data[1]
        goal.target_pose.pose.orientation.w = 1.0
        self.get_state(goal, data)

    def get_state(self, goal, data):
        self.client.send_goal(goal)
        rospy.loginfo("Sending goal...")

        # Wait for the result
        if self.client.get_state() == 1:
            rospy.loginfo("Planning...")

        self.client.wait_for_result()
        rospy.sleep(1.0)
        self.nav_result.publish(self.client.get_state())

        # Interprete results

        # Goal reached, send go signal to move_arm.py
        if self.client.get_state() == 3:
            self.nav_result.publish(1)
            rospy.loginfo("Reached!")
            sys.exit("set_nav_goal.py terminated.")

        # Goal cancelled, exit.
        elif self.client.get_state() == 2:
            rospy.loginfo("Navigation has been cancelled.")
            sys.exit("set_nav_goal.py terminated.")

        # Goal cannot be reached, try other orientations otherwise cancel navigation
        elif self.client.get_state() == 4:
            rospy.loginfo("Object not reachable. Trying new orientation...")
            self.change_orientation(goal, data)

    def change_orientation(self, goal, data):
        # Different orientations that can be tried in sets of 45 degrees
        q0 = Quaternion(0, 0, 1, 0)
        q45 = Quaternion(0, 0, -0.9238795, 0.3826834) 
        q90 = Quaternion(0, 0, -0.7071068, 0.7071068)
        q135 = Quaternion(0, 0, -0.3826834, 0.9238795)  
        q180 = Quaternion(0,0,0,1)  
        q225 = Quaternion(0, 0, 0.3826834, 0.9238795)
        q270 = Quaternion(0, 0, 0.7071068, 0.7071068)
        q315 = Quaternion(0, 0, 0.9238795, 0.3826834)
        circle_orientation = [q0,q45,q90,q135,q180,q225,q270,q315]
        circle_distance = 0        #in degrees

        
        #Only trying different orientations
        for q in circle_orientation:

            if self.client.get_state() == 4 and len(tries) <= 9:
                # Keep track how many options are tried
                tries.append(circle_distance)
                print("Tried: ", tries) 

                # Cancel old goal, change goal
                self.client.cancel_goal()
                goal.target_pose.pose.orientation = q
                goal.target_pose.pose.position.x = data[0] + 0.3*np.cos(np.deg2rad(circle_distance))
                goal.target_pose.pose.position.y = data[1] + 0.3*np.sin(np.deg2rad(circle_distance))

                # Send new goal, wait for result
                self.client.send_goal(goal)
                self.client.wait_for_result()
                rospy.sleep(1.0)
                self.nav_result.publish(self.client.get_state())

                #Add 45 degrees if still not reached
                circle_distance += 45

            else:
                del tries[:]
                break        

        # give message and continue to next object or normal routine
        if self.client.get_state() == 3:
            self.client.cancel_goal()
            self.nav_result.publish(1)
            rospy.loginfo("Goal is reached after changing orientation")
            sys.exit("set_nav_goal.py terminated")

        elif self.client.get_state() == 2:
            rospy.loginfo("Navigation has been cancelled by operator.")
            sys.exit("set_nav_goal.py terminated.")

        else:
            self.client.cancel_goal()
            rospy.loginfo("Object still not reachable after trying other orientations. Goal cancelled.")
            sys.exit("set_nav_goal.py terminated")


def main(args):
    rospy.init_node("set_nav_goal", anonymous=True)
    set_nav_goal()
    rospy.spin()


# call main
if __name__ == "__main__":
    print("set_nav_goal.py started")
    main(sys.argv)
    print("set_nav_goal.py terminated")
