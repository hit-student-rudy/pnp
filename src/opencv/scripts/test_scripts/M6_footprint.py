#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf2_ros 
import tf
import geometry_msgs.msg
import tf2_geometry_msgs
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, Quaternion
from std_msgs.msg import String, Float64 

class create_footprint_frame:

    #initiating the subscriptions and publishers
    def __init__(self):
        rospy.Subscriber('object_pose_robot', Pose, self.callback_link)
        self.tl = tf.TransformListener()
        self.br = tf.TransformBroadcaster()
        self.send_bf = rospy.Publisher('object_pose_base_link', PoseStamped, queue_size=10)
        rospy.sleep(3)

    def callback_link(self, data):
        pt = PoseStamped()
        pt.header.frame_id = "xtion_optical_frame"
        pt.pose.position = data.position
        pt.pose.orientation = data.orientation
        pt.header.stamp = rospy.Time(0)
        bf = self.tl.transformPose('base_link',pt)
        _, rot = self.tl.lookupTransform('map','base_footprint', rospy.Time(0))
        bf.pose.orientation =  Quaternion(x=rot[0],y=rot[1],z=rot[2],w=rot[3])
        self.send_bf.publish(bf) 

        t = TransformStamped()
        t.header.frame_id = 'base_link'
        t.child_frame_id = 'object_pose_base_footprint'
        t.header.stamp = rospy.Time.now()
        t.transform.translation = bf.pose.position
        t.transform.rotation = bf.pose.orientation
        self.br.sendTransformMessage(t)



def main(args):
  rospy.init_node('create_map_frame', anonymous=True)
  create_footprint_frame()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("footprint_frame.py started")
    main(sys.argv)
    print("footprint_frame.py terminated")
    