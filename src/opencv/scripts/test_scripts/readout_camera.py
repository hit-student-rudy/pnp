#!/usr/bin/env python

#import all of the needed libraries
from __future__ import print_function
import roslib
roslib.load_manifest('opencv')
import sys
import rospy
import cv2
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

class class_name:

  #initiating the subscriptions and publishers
  def __init__(self):
    self.bridge = CvBridge()
    self.image_rgb = rospy.Subscriber("xtion/rgb/image_raw",Image,self.callback_rgb) 
    self.image_depth = rospy.Subscriber("xtion/depth_registered/image_raw",Image,self.callback_depth)
  
  #Depth needs it's own callback to process the image for OpenCV, use 'self.' to use it in other def's
  def callback_depth(self,data):
    self.cv_depth= self.bridge.imgmsg_to_cv2(data,  desired_encoding="passthrough")

  #Callback for rgb and body of the code
  def callback_rgb(self,data):
    cv_rgb = self.bridge.imgmsg_to_cv2(data, 'bgr8')

    #showing the video output
    cv2.imshow("RGB", cv_rgb)
    cv2.imshow("Depth", self.cv_depth)
    cv2.waitKey(3)

  
def main(args):
  class_name()
  rospy.init_node('PUT_NODE_NAME_HERE', anonymous=True)
  rospy.spin()  
  cv2.destroyAllWindows()
  print("PROGRAM TERMINATED")

#call main
if __name__ == '__main__':
    main(sys.argv)
    
    

