#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf2_ros 
import tf
import geometry_msgs.msg
import tf2_geometry_msgs
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, Quaternion
from std_msgs.msg import String, Float64 

class show_frame:

    #initiating the subscriptions and publishers
    def __init__(self):
        self.br = tf.TransformBroadcaster()
        self.callback()
        
    def callback(self):
        q45 = Quaternion(0, 0, 0.3826834, 0.9238795)
        #q90 =  Quaternion( 0, 0, 0.7071068, 0.7071068)
        #q135 = Quaternion( 0, 0, 0.9238795, 0.3826834)
        #q180 = Quaternion( 0, 0, 1, 0)

        #Create a markerframe of the transformed object 
        t = TransformStamped()
        t.header.frame_id = 'map'
        t.child_frame_id = 'test_frame'
        t.header.stamp = rospy.Time.now()

        #Assign translation and rotation from opt to the markerframe
        t.transform.translation.x = 2.0
        t.transform.translation.y = 1.0
        t.transform.translation.z = 0.0
        t.transform.rotation = q45

        #Send the transformation to tf. Needed for readout in RVIZ
        self.br.sendTransformMessage(t)
        print(t)

def main(args):
  rospy.init_node('show_frame', anonymous=True)
  show_frame()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("show_frame.py started")
    main(sys.argv)
    print("show_frame.py terminated")
    