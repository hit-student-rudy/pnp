#!/usr/bin/env python
#only moving the arm towards the position
from __future__ import print_function
import roslib
import rospy
import actionlib
import sys
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
from std_msgs.msg import Bool, Int16, String


 

closed  = [0.045, 0.045]

class move_gripper:

    #initiating and check
    def __init__(self):

       
        self.joint_names = ["gripper_left_finger_joint", "gripper_right_finger_joint"]
        rospy.loginfo("Waiting for gripper_controller...")
        self.client = actionlib.SimpleActionClient("gripper_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
        self.client.wait_for_server()
        rospy.loginfo("...connected.")

        rospy.wait_for_message("joint_states", JointState)

        self.callback_go(True)


    def callback_go(self, data):
        while data == False:
            rospy.loginfo("Not yet....")

        openGripper = [0.045, 0.045]
        closeGripper = [0.001, 0.001]

        self.moveGripper(openGripper)
        self.moveGripper(closeGripper)
        

    def moveGripper(self, move):
        trajectory = JointTrajectory()
        trajectory.joint_names = self.joint_names
        trajectory.points.append(JointTrajectoryPoint())
        trajectory.points[0].positions = move
        trajectory.points[0].velocities = [0.1 for i in self.joint_names]
        trajectory.points[0].accelerations = [0.1 for i in self.joint_names]
        trajectory.points[0].time_from_start = rospy.Duration(2.0)

        rospy.loginfo("moveing gripper...")
        goal = FollowJointTrajectoryGoal()
        goal.trajectory = trajectory
        goal.goal_time_tolerance = rospy.Duration(0.0)

        self.client.send_goal(goal)
        self.client.wait_for_result(rospy.Duration(3.0))
        rospy.loginfo("Gripper moved.")



def main(args):
  rospy.init_node('move_gripper', anonymous=True)
  move_gripper()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("move_gripper.py started")
    main(sys.argv)
    print("move_gripper.py terminated")
    