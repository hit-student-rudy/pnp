#!/usr/bin/env python
#rosrun opencv plot_filter_output.py
from __future__ import print_function
import roslib
import sys
import rospy
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from geometry_msgs.msg import PoseStamped, Pose


class plot_filter_output:

    #initiating the subscriptions and publishers
    def __init__(self):
        rospy.Subscriber('opt_filtered_mav', Pose, self.callback_maf)
        rospy.Subscriber('opt_filtered_exp', Pose, self.callback_esf)
        rospy.Subscriber('opt', PoseStamped, self.callback_map)
        self.maps,self.maf, self.esf, self.times= [], [], [], []
        
    def callback_maf(self, data):
        self.xa = round(data.position.x, 4)

    def callback_esf(self, data):
        self.xe = round(data.position.x, 4)

    def callback_map(self, data):
        self.xm = round(data.pose.position.x, 4)
        t = np.arange(0.0,1.1,0.02)

        if len(self.maf) != len(t):
            
            self.maf.append(self.xm)
            self.esf.append(self.xe)
            self.maps.append(self.xa)
            self.times.append(time.time())

        else:
            self.show()

    def show(self):
        rospy.loginfo("Plotting it..")
        plt.plot(self.times[:40], self.esf[:40], 'b-')
        plt.plot(self.times[:40], self.maps[:40], 'g-')
        plt.plot(self.times[:40], self.maf[:40], 'r-')
        
        plt.xlabel("Time (rostime)")
        plt.ylabel("Object movement (m)")

        blue_patch = mpatches.Patch(color='blue', label='Exponential smoothing')
        red_patch = mpatches.Patch(color='red', label='Map coordinates')
        green_patch = mpatches.Patch(color='green', label='Moving average')
        plt.legend(handles=[red_patch,green_patch, blue_patch])

        plt.show()

def main(args):
  rospy.init_node('plot_filter_output', anonymous=True)
  plot_filter_output()
  rospy.spin()
  plt.close()

#call main
if __name__ == '__main__':
    print('plot_filter_output.py started')
    main(sys.argv)
    print('plot_filter_output.py terminated')