#!/usr/bin/env python
#only moving the arm towards the position
from __future__ import print_function
import roslib
import rospy
import sys
import copy
import moveit_commander
import moveit_msgs.msg
from moveit_msgs.msg import DisplayTrajectory
from moveit_commander import RobotCommander, PlanningSceneInterface, MoveGroupCommander, roscpp_initialize
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from std_msgs.msg import Bool, Int16

class move_arm:

    #initiating and check
    def __init__(self):

        rospy.loginfo("Starting setup...")
        roscpp_initialize(sys.argv)
        self.robot = RobotCommander()
        self.scene = PlanningSceneInterface()
        self.move_group = MoveGroupCommander('arm_torso')
        
        self.pub_trajectory = rospy.Publisher('/move_group/display_planned_path', DisplayTrajectory, queue_size=2)
        
        print("Reference frame: ", self.move_group.get_planning_frame())
        print("Group names: ", self.robot.get_group_names())
        rospy.loginfo("Setup completed.")

        self.callback_point()
        self.callback_go(True)

    #Moving to a pose goal
    def callback_point(self):
        self.x = 0.35
        self.y = 0.35

    def callback_go(self, data):
        while data == False:
            rospy.loginfo("Not yet....")

        #Plan motion
        rospy.loginfo("Generating first plan...")
        pose_target = Pose()
        pose_target.orientation.w = 0.7071068
        pose_target.orientation.y = 0.7071068
        pose_target.position.x = self.x
        pose_target.position.y = self.y
        pose_target.position.z = 0.27
        self.move_group.set_pose_target(pose_target)

        #Compute and execute
        self.move_group.go(wait=True)
        self.move_group.stop() #Call stop to make no movement
        self.move_group.clear_pose_targets()

def main(args):
  rospy.init_node('move_arm', anonymous=True)
  move_arm()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("move_arm.py started")
    main(sys.argv)
    print("move_arm.py terminated")
    