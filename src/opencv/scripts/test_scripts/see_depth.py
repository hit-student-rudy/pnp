#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('opencv')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String, Float64
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

class image_converter:

  def __init__(self):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/depth/image",Image,self.callback)
    self.send_depth = rospy.Publisher('depth_in_metres', Float64, queue_size=100)
 


  def callback(self,data):
    cv_image = self.bridge.imgmsg_to_cv2(data,  desired_encoding="passthrough") 
    self.depth = cv_image[(640/2),(480/2)]
    rospy.loginfo(self.depth)
    self.send_depth.publish(self.depth)
    cv2.imshow('Asus Xtion', cv_image)
    cv2.waitKey(3)


def main(args):
  image_converter()
  rospy.init_node('depth_image', anonymous=True)
  rospy.spin()  
  cv2.destroyAllWindows()
  
if __name__ == '__main__':
    main(sys.argv)
    
 
