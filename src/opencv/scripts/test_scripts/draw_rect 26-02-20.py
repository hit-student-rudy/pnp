#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('opencv')
import sys
import time
import rospy
import cv2
import math
import numpy as np
import tf_conversions
import tf2_ros
from geometry_msgs.msg import Point, Quaternion, Pose
from visualization_msgs.msg import Marker, MarkerArray
from sensor_msgs.msg import Image
from std_msgs.msg import String, Float64 

from cv_bridge import CvBridge

class image_converter:

  def __init__(self):
    self.bridge = CvBridge()
    self.image_rgb = rospy.Subscriber("camera/rgb/image_raw",Image,self.callback_rgb)
    self.image_depth = rospy.Subscriber("camera/depth_registered/image_raw",Image,self.callback_depth)
    self.tf = rospy.Subscriber("/tf",Float64,self.callback_tf)
    self.send_point = rospy.Publisher('point', Point, queue_size=100 )
    self.send_pose = rospy.Publisher('pose', Pose, queue_size=100 )
    self.marker_pub = rospy.Publisher("markers", MarkerArray, queue_size=10)
  
  def callback_depth(self,data):
    self.cv_depth= self.bridge.imgmsg_to_cv2(data,  desired_encoding="passthrough")

  def callback_rgb(self,data):
    cv_rgb = self.bridge.imgmsg_to_cv2(data, 'bgr8') 
    hsv = cv2.cvtColor(cv_rgb, cv2.COLOR_BGR2HSV)
    lower_hsv = np.array([15, 100, 80])
    higher_hsv = np.array([38, 255, 255])
    mask = cv2.inRange(hsv, lower_hsv, higher_hsv)
    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT,(1,1)), iterations=10)
    contour = cv2.findContours(mask.copy(), 
                                cv2.RETR_EXTERNAL, 
                                cv2.CHAIN_APPROX_SIMPLE) [-2]
    for cnt in contour:
      if  cv2.contourArea(cnt) > 1000 :
        area = max(contour, key=cv2.contourArea)
        quat_tf = [0, 0, 1, 0]
        xg,yg,wg,hg = cv2.boundingRect(area)
        cv2.rectangle(cv_rgb,(xg,yg),(xg+wg, yg+hg),(0,255,0),2)

        self.x_px = xg+(wg/2)
        self.y_px = yg+(hg/2)
        self.z = self.cv_depth[self.y_px,self.x_px]
        cv2.circle(cv_rgb, (self.x_px,self.y_px), 1,(255,50,0),2)

        x = (self.x_px - 319.5)/570.3422241210938*self.z
        y = (self.y_px - 329.5)/570.3422241210938*self.z

        point = Point(x=x,y=y,z=self.z )
        self.pose = Pose(position = point, orientation = Quaternion(quat_tf[0], quat_tf[1], quat_tf[2], quat_tf[3]))

        self.send_point.publish(point) 
        self.send_pose.publish(self.pose) 

    cv2.imshow("RGB", cv_rgb)
    cv2.imshow("mask", mask)
    cv2.imshow("Depth", self.cv_depth)
    cv2.waitKey(2)

  def callback_tf(self, data):
    mbr = data.TransformBroadcaster()
    mbr.sendTransform(self.pose.position, self.pose.orientation, "map", "RGB")

  
def main(args):
  image_converter()
  rospy.init_node('get_coordinates', anonymous=True)
  rospy.spin()  
  cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
    
    

