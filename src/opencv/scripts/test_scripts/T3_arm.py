#!/usr/bin/env python
from __future__ import print_function
import roslib
import rospy
import sys
import copy
import actionlib
from moveit_msgs.msg import DisplayTrajectory
from moveit_commander import RobotCommander, PlanningSceneInterface, MoveGroupCommander, roscpp_initialize
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from std_msgs.msg import Bool
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState

class reach_object:

    #initiating and check
    def __init__(self):

        #Set up sub and pubs
        rospy.loginfo("Starting setup...")
        roscpp_initialize(sys.argv)
        self.nav_place = rospy.Publisher('nav_place', Bool, queue_size=2)

        #Connecting head and torso
        self.robot = RobotCommander()
        self.scene = PlanningSceneInterface()
        self.move_arm = MoveGroupCommander('arm_torso')
        self.move_head= MoveGroupCommander('head')
        
        #Connecting gripper
        self.joints_gripper = ["gripper_left_finger_joint", "gripper_right_finger_joint"]
        self.gripper_client = actionlib.SimpleActionClient("gripper_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
        self.gripper_client.wait_for_server()

        #Connecting head
        self.joints_head = ["head_1_joint","head_2_joint"]
        self.head_client = actionlib.SimpleActionClient("head_controller/follow_joint_trajectory", FollowJointTrajectoryAction)
        self.head_client.wait_for_server()
        rospy.loginfo("...connected.")
        #rospy.wait_for_message("joint_states", JointState)
        
        #rospy.Subscriber('object_pose_base_link', PoseStamped, self.callback_gripper_xy_goal)
        #rospy.Subscriber('object_pose_map', PoseStamped, self.callback_gripper_z_goal)
        #rospy.Subscriber('nav_result', Bool, self.callback_pick_object)
        #rospy.Subscriber('drop_it', Bool,self.drop_it)
        self.drop_object_trigger = False
        rospy.loginfo("Setup completed.")

        
        self.callback_gripper_z_goal(0.5)
        self.callback_gripper_xy_goal([0.4,0.4])
        self.callback_pick_object(True)

    def callback_gripper_xy_goal(self, xy_goal):
        self.gripper_x = xy_goal[0]
        self.gripper_y = xy_goal[1]
    
    def callback_gripper_z_goal(self, object_point):
        self.gripper_z = object_point

    def callback_pick_object(self, go_signal):
        while go_signal == False:
            rospy.loginfo("Not yet....")

        tries = 0
        object_grasped = False
        x = 0.0

        #Head positions
        look_down = [0.0,-0.8]
        look_up = [0.0,0.0]

        #Gripper positions
        openGripper = [0.045, 0.045]
        closeGripper = [0.001, 0.001]
        gripper_above_object = 0.4
        rospy.sleep(2)
        
        if self.gripper_z < 0.27:
            gripper_at_object = 0.27
        else:
            gripper_at_object = self.gripper_z

        #Arm positions and orientations
        grasp_orientation = Quaternion(x=0,y=0.7071068,z=0,w=0.7071068)
        fold = [0.25,-.2,0.61]
        retreat = [0.5,0,0.9]
        retreat_orientation = Quaternion(x=0,y=-0.7071068,z=0,w= 0.7071068 )
        self.adjust_arm(retreat, retreat_orientation)
        
        #Plan motion
        #while object_grasped == False and tries < 3:
        #    self.look_at_object([x, -.9])
        #    
        #    #Place the arm above the object with opened gripper
        #    above_object = [self.gripper_x, self.gripper_y, gripper_above_object]
        #    print(above_object)
        #    self.adjust_arm(above_object, grasp_orientation)
        #    self.move_gripper(openGripper)
        #    rospy.sleep(2)
#
        #    #Move gripper around the object and close the gripper
        #    at_object = [self.gripper_x,self.gripper_y, gripper_at_object]
        #    print(at_object)
        #    self.adjust_arm(at_object, grasp_orientation)
        #    rospy.sleep(2)
        #    self.move_gripper(closeGripper)
#
        #    #Look at the object and move the arm
        #    self.adjust_arm(retreat, retreat_orientation)
        #    rospy.sleep(1)
#
        #    #Check if the object is grasped and try again or quit.
        #    self.look_at_object(look_down)
        #    object_grasped = self.check_object(at_object[2], self.gripper_z)
        #    
        #    tries += 1
#
        #    if tries == 1: x = .5
        #    if tries == 2: x = -.5
#
        #self.look_at_object(look_up)    
        rospy.loginfo("M5_reach.py finished")
        sys.exit()

    def look_at_object(self, position):
        head_trajectory = JointTrajectory()
        head_trajectory.joint_names = self.joints_head
        head_trajectory.points.append(JointTrajectoryPoint())
        head_trajectory.points[0].positions = position
        head_trajectory.points[0].velocities = [0.1 for i in self.joints_head]
        head_trajectory.points[0].accelerations = [0.1 for i in self.joints_head]
        head_trajectory.points[0].time_from_start = rospy.Duration(2.0)

        head_goal = FollowJointTrajectoryGoal()
        head_goal.trajectory = head_trajectory
        head_goal.goal_time_tolerance = rospy.Duration(0.0)

        self.head_client.send_goal(head_goal)
        self.head_client.wait_for_result(rospy.Duration(3.0))
        return()

    def move_gripper(self, move):
        gripper_trajectory = JointTrajectory()
        gripper_trajectory.joint_names = self.joints_gripper
        gripper_trajectory.points.append(JointTrajectoryPoint())
        gripper_trajectory.points[0].positions = move
        gripper_trajectory.points[0].velocities = [0.1 for i in self.joints_gripper]
        gripper_trajectory.points[0].accelerations = [0.1 for i in self.joints_gripper]
        gripper_trajectory.points[0].time_from_start = rospy.Duration(2.0)

        gripper_goal = FollowJointTrajectoryGoal()
        gripper_goal.trajectory = gripper_trajectory
        gripper_goal.goal_time_tolerance = rospy.Duration(0.0)

        self.gripper_client.send_goal(gripper_goal)
        self.gripper_client.wait_for_result(rospy.Duration(3.0))
        return()

    def adjust_arm(self, position, orientation):
        #Plan motion
        pose_target = Pose()
        pose_target.orientation = orientation
        pose_target.position.x = position[0]
        pose_target.position.y = position[1]
        pose_target.position.z = position[2]
        self.move_arm.set_pose_target(pose_target)

        #Compute and execute
        self.move_arm.go(wait=True)
        self.move_arm.stop() #Call stop to make no movement
        self.move_arm.clear_pose_targets()
        return()

    def check_object(self, gripper, depth):
        if(depth > 0.27):
            rospy.loginfo("Object successfully grasped")
            self.look_at_object([.0,-.3])
            self.nav_place.publish(True)
            self.drop_object()
            return(True)
        else:
            rospy.loginfo("Object is not grasped")
            return(False) 
    
    def drop_it(self, data):
        self.drop_object_trigger = data

    def drop_object(self):
        drop_position = [0.5,-0.2,0.5]
        drop_orientation = Quaternion(x=0,y=0.7071068,z=0,w=0.7071068)

        while self.drop_object_trigger == False:
            rospy.loginfo("Not at drop off location yet")

        else:
            rospy.loginfo("DROP THAT BASSSS dumdumdum")
            self.look_at_object([0.0,-.8])
            self.adjust_arm(drop_position, drop_orientation)
            rospy.sleep(2)
            self.move_gripper([0.045, 0.045])
            rospy.sleep(2)
            self.adjust_arm([0.0,0.4,1.0],Quaternion(x=0.0,y=0.7071068,z=0,w=0.7071068) )
            sys.exit()
            return()

def main(args):
  rospy.init_node('reach_object', anonymous=True)
  reach_object()
  rospy.spin()
  sys.exit()  

#call main
if __name__ == '__main__':
    print("M5_reach.py started")
    main(sys.argv)
    sys.exit()
    print("M5_reach.py terminated")