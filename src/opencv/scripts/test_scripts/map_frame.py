#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf2_ros 
import tf
import geometry_msgs.msg
import tf2_geometry_msgs
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped, Quaternion
from std_msgs.msg import String, Float64 

class create_map_frame:

    #initiating the subscriptions and publishers
    def __init__(self):
        rospy.Subscriber('object_pose_robot', Pose, self.callback)
        self.tl = tf.TransformListener()
        self.br = tf.TransformBroadcaster()
        self.send_opt = rospy.Publisher('object_pose_map', PoseStamped, queue_size=10)
        rospy.sleep(3)

    def callback(self, data):
        #Copy the pose, make it PoseStamped to transform it from camera frame to map
        tpt = PoseStamped()
        tpt.header.frame_id = "xtion_optical_frame"
        #Assign the positions and orientation of the object, opt is the object with the map coordinates
        tpt.pose.position = data.position
        tpt.pose.orientation = data.orientation
        tpt.header.stamp = rospy.Time(0)
        opt = self.tl.transformPose('map',tpt)
        _, rot = self.tl.lookupTransform('map','base_footprint', rospy.Time(0))
        opt.pose.orientation =  Quaternion(x=rot[0],y=rot[1],z=rot[2],w=rot[3])
        #opt.pose.orientation = Quaternion(0,0,0,1)
        
        #publish map coordinates on topic
        self.send_opt.publish(opt) 

        #Create a markerframe of the transformed object 
        tt = TransformStamped()
        tt.header.frame_id = 'map'
        tt.child_frame_id = 'object_pose_map'
        tt.header.stamp = rospy.Time.now()

        #Assign translation and rotation from opt to the markerframe
        tt.transform.translation = opt.pose.position
        tt.transform.rotation = opt.pose.orientation

        #Send the transformation to tf. Needed for readout in RVIZ
        self.br.sendTransformMessage(tt)
        #rospy.loginfo(t) #uncomment to show the map coordinates

def main(args):
  rospy.init_node('create_map_frame', anonymous=True)
  create_map_frame()
  rospy.spin()  
  

#call main
if __name__ == '__main__':
    print("map_frame.py started")
    main(sys.argv)
    print("map_frame.py terminated")
    