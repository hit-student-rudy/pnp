#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf
import actionlib
import numpy as np
from actionlib_msgs.msg import GoalStatusArray, GoalStatus
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseStamped, Pose, Quaternion, Point
from std_msgs.msg import Bool, Int16
tries = []

class set_nav_goal:

    #initiating the subscriptions and publishers
    def __init__(self):
        self.nav_result = rospy.Publisher('nav_result', Bool, queue_size=2)
        self.nav_arm = rospy.Publisher('arm_goal', Point, queue_size=2)
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        self.client.wait_for_server()
        rospy.loginfo('Connected to move_base.')
        rospy.Subscriber('object_pose_filtered', Pose, self.callback_pose) 
        

    #Create a goal for the base to move to, with half a metre distance from the middle of the object
    def callback_pose(self, data):
        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = 'map'
        goal.target_pose.header.stamp = rospy.Time.now()
        goal.target_pose.pose.position.x = data.position.x - 0.5
        goal.target_pose.pose.position.y = data.position.y
        goal.target_pose.pose.orientation = data.orientation

        arm_goal = Point()
        arm_goal.x = data.position.x
        arm_goal.y = data.position.y
        arm_goal.z = data.position.z
        self.get_state(goal, data, arm_goal) 

    #Get state of the send goal, act on result
    def get_state(self, goal, data, arm_goal):
        #Send goal
        self.client.send_goal(goal)
        rospy.loginfo("Sending goal...")

        #Wait for the result
        if self.client.get_state() == 1:
            rospy.loginfo("Planning...")

        self.client.wait_for_result()
        rospy.sleep(1.0)

        #Interprete results
        #Goal reached, send go signal to move_arm.py
        if self.client.get_state() == 3:
            self.nav_result.publish(True)
            self.nav_arm.publish(arm_goal)
            rospy.loginfo("Reached!")
            sys.exit('set_nav_goal.py terminated.') 

        #Goal cancelled, exit.
        elif self.client.get_state() == 2: 
            rospy.loginfo("Navigation has been cancelled.")
            sys.exit('set_nav_goal.py terminated.')

        #Goal cannot be reached, try other orientations and positions in change_goal
        elif self.client.get_state() == 4:
            rospy.loginfo("Object not reachable. Trying new orientation...")
            self.change_goal(goal,data,arm_goal)


    #Change goal, get state, act on new result
    def change_goal(self,goal,data, arm_goal):
        # Different orientations that can be tried in sets of 45 degrees
        q0 = Quaternion(0, 0, 1, 0)
        q45 = Quaternion(0, 0, -0.9238795, 0.3826834) 
        q90 = Quaternion(0, 0, -0.7071068, 0.7071068)
        q135 = Quaternion(0, 0, -0.3826834, 0.9238795)  
        q180 = Quaternion(0,0,0,1)  
        q225 = Quaternion(0, 0, 0.3826834, 0.9238795)
        q270 = Quaternion(0, 0, 0.7071068, 0.7071068)
        q315 = Quaternion(0, 0, 0.9238795, 0.3826834)
        circle_orientation = [q0,q45,q90,q135,q180,q225,q270,q315]
        circle_distance = 0 

        #Try the different orientation and position in a circle around the object. 
        for q in circle_orientation:

            if self.client.get_state() == 4 and len(tries) <= 9:
                #Keep track how many options are tried
                tries.append(circle_distance)
                print("Tried: ", tries) 

                #Cancel old goal, change goal
                self.client.cancel_goal()
                goal.target_pose.pose.orientation = q
                goal.target_pose.pose.position.x = data.position.x + 0.5*np.cos(np.deg2rad(circle_distance))
                goal.target_pose.pose.position.y = data.position.y + 0.5*np.sin(np.deg2rad(circle_distance))

                #Send new goal, wait for result
                self.client.send_goal(goal)
                self.client.wait_for_result()
                rospy.sleep(1.0)

                #Add 45 degrees if still not reached, afterwards because the first goal is distance 0.
                circle_distance += 45

            else:
                #Delete the content of tries, break from the loop
                del tries[:]
                break        
       
        #Goal reached, exit
        if self.client.get_state() == 3:
            self.client.cancel_goal()
            self.nav_result.publish(True)
            self.nav_arm.publish(arm_goal)
            rospy.loginfo("Goal is reached after changing orientation")
            sys.exit('set_nav_goal.py terminated')

        #Goal cancelled, exit
        elif self.client.get_state() == 2:
            self.client.cancel_goal()
            rospy.loginfo("Navigation has been cancelled by operator.")
            sys.exit("set_nav_goal.py terminated.")
        
        #Object not reachable, exit
        else:  
            self.client.cancel_goal() 
            rospy.loginfo("Object still not reachable after trying other orientations. Goal cancelled.")   
            sys.exit('set_nav_goal.py terminated') 
    
def main(args):
  rospy.init_node('set_nav_goal', anonymous=True)
  set_nav_goal()
  rospy.spin()  

#Call main
if __name__ == '__main__':
    print('set_nav_goal.py started')
    main(sys.argv)
    print('set_nav_goal.py terminated')