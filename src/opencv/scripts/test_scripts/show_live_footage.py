#!/usr/bin/env python
from __future__ import print_function

import roslib
roslib.load_manifest('opencv')
import sys
import rospy
import cv2
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError


class image_converter:

  def __init__(self):
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("camera/rgb/image_raw",Image,self.callback)
	

  def callback(self,data):
    cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    cv2.imshow("Asus Xtion", cv_image)
    cv2.waitKey(3)

def main(args):
  image_converter()
  rospy.init_node('show_live_footage', anonymous=True)
  rospy.spin()  
  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

