#!/usr/bin/env python
#This code filters the output and makes a tf marker for this pose, it publishes the pose the 'opt_filtered' topic
from __future__ import print_function
import roslib
import sys
import rospy
import tf2_ros 
import tf
import numpy as np
from math import sqrt
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionGoal
from geometry_msgs.msg import PoseStamped, TransformStamped, Pose, Point, Quaternion

tx,ty,tz = [],[],[]

class filter_pose:
    

    #initiating the subscriptions and publishers
    def __init__(self):
        self.tl = tf.TransformListener()
        self.br = tf.TransformBroadcaster()
        self.send_pose = rospy.Publisher('opt_filtered_mav', Pose, queue_size=12)
        rospy.Subscriber('opt', PoseStamped, self.callback)
    def callback(self, data):
        #get live orientation 
        quat = data.pose.orientation

        #Save 5 poses to make it possible to take a mean
        if len(tx)<6:
            tx.append(data.pose.position.x)
            ty.append(data.pose.position.y)
            tz.append(data.pose.position.z)

        #when 5 poses are saved go to make_pose
        else: 
            #Send the transformation to tf. Needed for readout in RVIZ
            print("SENDING THE FILTERED POSE")
            point = self.make_pose(tx,ty,tz, quat)
            t = self.make_frame(point, quat)
            self.br.sendTransformMessage(t)
            
            #Delete first item in the list
            del tx[0],ty[0],tz[0]

    def make_pose(self,tx,ty,tz, quat):
            #take mean of point and quat and make a pose, publish for nav goal
            #point = Point(x=round(np.mean(np.array(tx)), 4), y=round(np.mean(np.array(ty)), 4), z=round(np.mean(np.array(tz)), 4))
            point = Point(x=np.mean(np.array(tx)), y=np.mean(np.array(ty)), z=np.mean(np.array(tz)))
            pose_filtered = Pose(position = point, orientation = quat) 
            self.send_pose.publish(pose_filtered)
            return(point)
            

    def make_frame(self, point, quat):
        print('in send_frame')

        #Create a markerframe of the transformed object 
        t = TransformStamped()
        t.header.frame_id = 'map'
        t.child_frame_id = 'map_object_filtered'
        t.header.stamp = rospy.Time.now()
        
        #Assign translation and rotation from opt to the markerframe (in mm)
        t.transform.translation = point
        t.transform.rotation = quat

        rospy.loginfo(t) #uncomment to show the map coordinates
        return(t)

   

def main(args):
  rospy.init_node('filter_pose', anonymous=True)
  filter_pose()
  rospy.spin()  

#call main
if __name__ == '__main__':
    print('filter_pose.py started')
    main(sys.argv)
    print('filter_pose.py terminated')