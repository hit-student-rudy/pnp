#!/usr/bin/env python
#This code filters the output and makes a tf marker for this pose, it publishes the pose the 'opt_filtered' topic
from __future__ import print_function
import roslib
import sys
import rospy
import tf
import numpy as np
from math import sqrt
from move_base_msgs.msg import MoveBaseGoal, MoveBaseAction, MoveBaseActionGoal
from geometry_msgs.msg import PoseStamped, TransformStamped, Pose, Point

class filter_pose:
    

    #initiating the subscriptions and publishers
    def __init__(self):
        self.iter_x, self.iter_y, self.iter_z = 0,0,0
        self.smoothed_x, self.smoothed_y, self.smoothed_z = 0,0,0
        self.tb = tf.TransformBroadcaster()
        self.send_pose = rospy.Publisher('object_pose_filtered', Pose, queue_size=10)
        rospy.Subscriber('object_pose_map', PoseStamped, self.callback)
       

    def callback(self, data):
        #get live orientation 
        quat = data.pose.orientation

        #Smooth the point
        filtered_x = self.filter_x(data.pose.position.x, self.iter_x)
        filtered_y = self.filter_y(data.pose.position.y, self.iter_y)
        filtered_z = self.filter_z(data.pose.position.z, self.iter_z)

        #Send the transformation to tf. Needed for readout in RVIZ
        point = Point(x = filtered_x, y = filtered_y, z = filtered_z)
        pose_filtered = Pose(position = point, orientation = quat)
        self.send_pose.publish(pose_filtered)
        self.make_frame(point, quat)
        print(point)

    def make_frame(self, point, quat):
        filtered_marker = TransformStamped()
        filtered_marker.header.frame_id = 'map'
        filtered_marker.child_frame_id = 'object_pose_filtered'
        filtered_marker.header.stamp = rospy.Time.now()
        filtered_marker.transform.translation = point
        filtered_marker.transform.rotation = quat
        self.tb.sendTransformMessage(filtered_marker)
        return()

    def filter_x(self,X,i):
        if i == 0:
            old = X
        else: 
            old = self.smoothed_x
        
        self.smoothed_x = old + 0.3*(X - old)
        self.iter_x = 1
        return self.smoothed_x

    def filter_y(self,Y,i):
        if i == 0:
            old = Y
        else: 
            old = self.smoothed_y
        
        self.smoothed_y = old + 0.3*(Y - old)
        self.iter_y = 1
        return self.smoothed_y

    def filter_z(self,Z,i):
        if i == 0:
            old = Z
        else: 
            old = self.smoothed_z
        
        self.smoothed_z = old + 0.3*(Z - old)
        self.iter_z = 1
        return self.smoothed_z

def main(args):
  rospy.init_node('M3_filter_pose', anonymous=True)
  filter_pose()
  rospy.spin()  

#call main
if __name__ == '__main__':
    print('M3_filter_pose.py started')
    main(sys.argv)
    print('M3_filter_pose.py terminated')