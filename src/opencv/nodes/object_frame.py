#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import tf_conversions
import tf2_ros
import geometry_msgs.msg
from geometry_msgs.msg import TransformStamped, Pose
from tf import TransformListener

class create_object_frame:
    #initiating the subscriptions and publishers
    def __init__(self):
        rospy.Subscriber('pose', Pose, self.callback_frame)
        self.br = tf2_ros.TransformBroadcaster()

        
    def callback_frame(self, data):
        #Create a markerframe of the detected object        
        t = TransformStamped()
        t.header.stamp = rospy.Time(0)
        t.header.frame_id = 'xtion_optical_frame'
        t.child_frame_id = 'detected_object'

        #Assign translation and rotation from opt to the markerframe
        t.transform.translation.x = data.position.x
        t.transform.translation.y = data.position.y
        t.transform.translation.z = data.position.z
        t.transform.rotation.x = data.orientation.x
        t.transform.rotation.y = data.orientation.y
        t.transform.rotation.z = data.orientation.z
        t.transform.rotation.w = data.orientation.w
        
        #Send the transformation to tf. Needed for readout in RVIZ
        self.br.sendTransform(t)
        print(t)


def main(args):
  create_object_frame()
  rospy.init_node('create_obtject_frame', anonymous=True)
  rospy.spin()

#call main
if __name__ == '__main__':
    main(sys.argv)
    