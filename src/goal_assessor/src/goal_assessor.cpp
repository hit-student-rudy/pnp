#include "goal_assessor.h"



void Goal_Assessor::init(){
    ros::NodeHandle nh_("~");
    
    // Subscribers
    goal_cost_sub_ = nh_.subscribe("/move_base/local_costmap/goal_assessment_layer/goal_cost",1000, &Goal_Assessor::cost_goal_Callback, this);
    goal_sub_ = nh_.subscribe("/move_base/goal",1000, &Goal_Assessor::Goal_Callback, this);

    goal_pub_ = nh_.advertise<std_msgs::Bool>("/goal_is_cancelled", 1);

    // Publishers
    cancel_goal_pub_ = nh_.advertise<actionlib_msgs::GoalID>("/move_base/cancel", 1);

    // Set parameters
    ros::NodeHandle priv_n("~");
    priv_n.param("cost_threshold", cost_threshold_, 170);

    goal_cancelled_ = false;
    
}


void Goal_Assessor::cost_goal_Callback(const std_msgs::Int32& goal_cost){
   cost_present_ = true; 
   goal_cost_ = goal_cost; 
}

void Goal_Assessor::Goal_Callback(const move_base_msgs::MoveBaseActionGoal goal){
   current_goal_ = goal; 
}

void Goal_Assessor::ReadGoalCost(){
    if (!cost_present_)
        return;
    else{
          
       if (old_goal_.header.stamp == current_goal_.header.stamp){
        //    ROS_INFO_STREAM("SAME GOAL");
           goal_cancelled_ == true;
       }
       else{
            // ROS_INFO_STREAM("DIFFERENT GOAL");
           goal_cancelled_ == false;
       }

       if ((goal_cost_.data >= cost_threshold_) & (goal_cancelled_ == false) ){
            ROS_INFO_STREAM("CANCEL");
            goal_id_.stamp = ros::Time::now();
            goal_id_.id = " ";
            cancel_goal_pub_.publish(goal_id_);
            // ros::Duration(0.5).sleep();
            goal_cancelled_ = true;
            old_goal_.header.stamp = current_goal_.header.stamp;
            goal_is_cancelled_.data = true;
            goal_pub_.publish(goal_is_cancelled_);
       }
       else if(goal_cost_.data < cost_threshold_){
            goal_cancelled_ = false;
            goal_is_cancelled_.data = false;
            
      }
       else{
          goal_is_cancelled_.data = false;
        //    ROS_INFO_STREAM("ELSE"); 
        //    return;
       } 




    }
    // ROS_INFO_STREAM("Goal cancelled= " << goal_cancelled_); 
    goal_pub_.publish(goal_is_cancelled_);
}

// void Goal_Assessor::ReadGoalCost(){
//    if (!cost_present_)
//         return;
//    else{
//         if ((goal_cost_.data >= cost_threshold_) & (goal_cancelled_ == false) ){
//             goal_id_.stamp = ros::Time::now();
//             goal_id_.id = " ";
//             cancel_goal_pub_.publish(goal_id_);
//             // ros::Duration(0.5).sleep();
//             goal_cancelled_ = true;
//             }
//         else if(goal_cost_.data < cost_threshold_){
//             goal_cancelled_ = false;
//             // return;
//         }
//         else{
//             // goal_cancelled_ = false;
//             return;
//         }
            
//    }
//    ROS_INFO_STREAM("Goal cancelled= " << goal_cancelled_);
// }
    

int main(int argc, char** argv){

ros::init(argc, argv, "Read_goal_cost");

Goal_Assessor assessor;
assessor.init();

assessor.old_goal_.header.stamp = ros::Time::now()+ ros::Duration(0.1);

ros::Rate loop_rate(10);

while (ros::ok())
{
 
 assessor.ReadGoalCost();  
 
 loop_rate.sleep();
 ros::spinOnce();
} 


  return 0;
}