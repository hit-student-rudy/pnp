#ifndef GOAL_ASSESSOR_H_
#define GOAL_ASSESSOR_H_ 

#include <ros/ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <move_base_msgs/MoveBaseActionGoal.h>
#include <geometry_msgs/PoseStamped.h>
#include <move_base_msgs/MoveBaseGoal.h>
#include <actionlib_msgs/GoalID.h>
#include <std_msgs/Bool.h>



class Goal_Assessor{
 
 public: 
        void init();
        
        ros::Subscriber goal_cost_sub_; // subscriber for the cost of the robot's navigation goal 
        ros::Subscriber goal_sub_; // subscriber for the robot's navigation goal 
        
        
        ros::Publisher  cancel_goal_pub_; // Publisher to cancel the (blocked) goal
	ros::Publisher goal_pub_;
        
        actionlib_msgs::GoalID goal_id_; // variable to publish a cancel goal topic

        void cost_goal_Callback(const std_msgs::Int32& goal_cost); // robot goal cost callback function
        void Goal_Callback(const move_base_msgs::MoveBaseActionGoal goal);  // robot goal callback function


        void ReadGoalCost();
        
        std_msgs::Int32 goal_cost_; // cost of robot's goal
        move_base_msgs::MoveBaseActionGoal old_goal_, current_goal_; // robot's current goal

        bool cost_present_ ; // to check if actually a cost is published (if topic is empty)
        bool goal_cancelled_ ;

	std_msgs::Bool goal_is_cancelled_;

        int cost_threshold_; 
};
#endif
